package com.atrums.offline.feletronica.process;

import org.apache.log4j.Logger;

public class ATECOFF_ServiceRecibido {
  private String ambiente;
  private String fileString;
  private static Logger log = Logger.getLogger(ATECOFF_ServiceRecibido.class);

  public ATECOFF_ServiceRecibido(String ambiente, String fileString) {
    this.ambiente = ambiente;
    this.fileString = fileString;
  }

  public ATECOFF_SRIDocumentoRecibido CallRecibido() {
    ATECOFF_ServicioSRICall sriCall = new ATECOFF_ServicioSRICall(this.ambiente, this.fileString,
        null);

    ATECOFF_SRIDocumentoRecibido recibido = new ATECOFF_SRIDocumentoRecibido(
        sriCall.RecibidoCall());

    sriCall = null;

    log.info(recibido.getEstadoespecifico());

    return recibido;
  }
}
