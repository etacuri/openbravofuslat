//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CODatosRetencionData implements FieldProvider {
static Logger log4j = Logger.getLogger(CODatosRetencionData.class);
  private String InitRecordNumber="0";
  public String taxid;
  public String cBpartnerLocationId;
  public String dateacct;
  public String cBpartnerId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("taxid"))
      return taxid;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CODatosRetencionData[] select(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return select(connectionProvider, cInvoiceId, 0, 0);
  }

  public static CODatosRetencionData[] select(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    SELECT b.taxid,c.c_bpartner_location_id,c.dateacct,c.c_bpartner_id" +
      "    FROM c_invoice c, c_bpartner b " +
      "    WHERE  c.c_bpartner_id=b.c_bpartner_id" +
      "    AND c_invoice_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosRetencionData objectCODatosRetencionData = new CODatosRetencionData();
        objectCODatosRetencionData.taxid = UtilSql.getValue(result, "taxid");
        objectCODatosRetencionData.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectCODatosRetencionData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectCODatosRetencionData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectCODatosRetencionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosRetencionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosRetencionData objectCODatosRetencionData[] = new CODatosRetencionData[vector.size()];
    vector.copyInto(objectCODatosRetencionData);
    return(objectCODatosRetencionData);
  }
}
